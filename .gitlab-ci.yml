variables:
    GIT_SUBMODULE_STRATEGY: recursive
    BASE_TAG: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/${CI_COMMIT_REF_SLUG}

stages:
  - build
  - test
  - package
  - deploy

#default:
#  tags:
#    - docker

build_hugo:
  stage: build
  image: registry.gitlab.com/kicad/kicad-ci/hugo-build-env:2.0.0
  artifacts:
    paths:
    - public
  script:
    - hugo

preview_hugo:
  stage: build
  image: registry.gitlab.com/kicad/kicad-ci/hugo-build-env:2.0.0
  artifacts:
    paths:
      - public
    reports:
      dotenv: deploy.env
  script:
    - echo # actually build with hugo, this is not the same step as build hugo since we need slightly
    - echo # different hugo settings for the preview to work
    - hugo config --config config.toml,preview.toml
    - hugo --config config.toml,preview.toml
    - echo # unfortunately the URL where the assets are deployed differs depending on who runs the pipeline
    - echo # there are two cases, the CI can run in the project context, or in the contributors context
    - echo # we use the schema for a 'dynamic' URL from the manual
    - echo # https://docs.gitlab.com/ee/ci/environments/#set-a-dynamic-environment-url
    - if [ $CI_PROJECT_ROOT_NAMESPACE == "kicad" ]; then
    - DYNAMIC_ENVIRONMENT_URL="https://$CI_PROJECT_ROOT_NAMESPACE.$CI_PAGES_DOMAIN/-/libraries/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/index.html"
    - else
    - DYNAMIC_ENVIRONMENT_URL="https://$CI_PROJECT_ROOT_NAMESPACE.$CI_PAGES_DOMAIN/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/index.html"
    - fi
    - echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" >> deploy.env
  environment:
    name: preview/$CI_COMMIT_REF_NAME
    url: $DYNAMIC_ENVIRONMENT_URL

htmlproofer:
  stage: test
  allow_failure: true
  image:
    name: ruby:latest
    entrypoint: [""]
  script:
  - gem install html-proofer:5.0.3
  - htmlproofer ./public/ --checks Links,Images,Scripts,Favicon --check_sri --check_external_hash --enforce-https --ignore-status-codes 503 --allow_hash_href --ignore-files ./public/404.html

package:
  stage: package
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker info
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:latest" .
    - docker save "$CI_REGISTRY_IMAGE:latest" | gzip > kicad-klc_${CI_COMMIT_SHORT_SHA}.tar.gz
  artifacts:
    expire_in: 1 week
    paths:
      - kicad-klc_${CI_COMMIT_SHORT_SHA}.tar.gz

deploy:
  stage: deploy
  resource_group: production
  dependencies:
    - package
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker info
    - docker load < kicad-klc_${CI_COMMIT_SHORT_SHA}.tar.gz
  script:
    - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    - docker push "$CI_REGISTRY_IMAGE:latest"
  only:
    - master
