+++
title = "F4.3 Connected copper elements have the same pad number"
+++

Footprints that contain multiple pads or conductive elements that are *physically connected* require special attention:

. Multiple pads that are physically connected must share the same number
. Thermal vias (if present) must share the same number as the thermal pad to which they are connected


{{< klcimg src="F4.3_same_pad_number" title="Connected copper areas must have same pad number" >}}