+++
title = "F6.1 Footprint component type must be set to surface mount"
+++

The *Component type* must be set to `SMD` for surface-mount footprints. This is to ensure that these footprints are included in the *Footprint position (.pos)* file output.

* The checkboxes `Not in schematic`, `Exclude from position files` and `Exclude from BOM` must be unchecked
* To set the footprint placement type, open the *Footprint properties* window and select *Surface Mount* as indicated.

{{< klcimg src="F6.1" title="Set placement type to 'Surface mount'" >}}
