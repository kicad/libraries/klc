+++
title = "S2.2 Non-functional variations in part number should be replaced with wildcard"
+++

Manufacturer Part Numbers (MPN) often include variations that are _non-functional_ in terms of schematic entry and PCB layout.

Examples of parameters considered non-functional:

* Temperature rating of the component
* Packaging information (e.g. reel, tray, tape)
* RoHS / PbFree information

Examples of parameters considered _functional_ are those that affect the schematic or layout of the part:

* Electrical variations - these indicate important different characteristics (e.g. different supply voltages)
* Package options - these indicate different footprints and/or pinouts

To capture every possible combination of part variation would require a large number of derived symbols and is to be avoided.

. Where an MPN has options for such non-functional variations, these portions of the MPN should be replaced with the wildcard character (lowercase `x`).
.. If these non-functional parts of the MPN are at the end of the symbol name then the wildcard should be omitted.
. Functional variations should be captured in the symbol name.
. If there's only one functional variation of the part, the wildcard does not need to be applied
.. Example: `TPS3430` has only one variant: `TPS3430WDRCR`. So the name should be `TPS3430WDRC`, rather than `TPS3430xDRC`.
